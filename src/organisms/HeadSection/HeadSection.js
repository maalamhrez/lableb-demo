import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles as customStyles } from "./styles";

const styles = customStyles;

class HeadSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.header}>
        <img src="https://picsum.photos/200/300.jpg"></img>
      </div>
    );
  }
}

HeadSection.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(HeadSection);
