import React from "react";
import { withStyles } from "@material-ui/core/styles";
export const styles = (theme) => ({
  title: {
    alignItems: "center",
    fontSize: "3em",
    marginBottom: theme.spacing(5),
  },
  products: {
    width: "100%",
    position: "relative",
  },
});
