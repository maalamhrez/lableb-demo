import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Product from "../../components/Product/Product";
import { styles as customStyles } from "./styles";
const styles = customStyles;

const mockedProducts = [
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
  "https://picsum.photos/200/300.jpg",
];
class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;

    return (
      <div>
        <div className={classes.title}>
          Try Lableb Search On The Following Websites:
        </div>
        <div className={classes.products}>
          {mockedProducts.map((imageLink, index) => {
            return <Product src={imageLink} />;
          })}
        </div>
      </div>
    );
  }
}

Products.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Products);
