import React from "react";
import { withStyles } from "@material-ui/core/styles";
export const styles = (theme) => ({
  root: {
    width: "100%",
    backgroundColor: "black",
    color: "white",
    display: "flex",
    fontFamily: "Arial, Helvetica, sans-serif",
  },
  description: {
    marginLeft: "10%",
    writingMode: "horizontal-rl",
    wordBreak: "break-word",
    webkitWritingMode: "vertical-rl",
    overflow: "hidden",
    lineHeight:"1.6",
    width: "300px",
    textAlign: "left",
  },
  links: {
    writingMode: "horizontal-rl",
    wordBreak: "break-word",
    webkitWritingMode: "vertical-rl",
    overflow: "hidden",
    textAlign: "left",
  },
  lablebTitle: {
    paddingTop: "1%",
    marginLeft: "10%",
    writingMode: "horizontal-rl",
    wordBreak: "break-word",
    overflow: "hidden",
    width: "300px",
    textAlign: "left",
  },
  col: {
    display: "inline-block",
  },
  linksHeader: {
    marginLeft: "300px",
    marginTop: "27%",
  },
  contactInfo: {
    writingMode: "horizontal-rl",
    wordBreak: "break-word",
    marginLeft: "300px",
    marginTop: "25%",
  },
  contactUs:{
    color:"cyan"
  }
});
