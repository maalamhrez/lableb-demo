import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { styles as customStyles } from "./styles";

const styles = customStyles;

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.col}>
          <div className={classes.lablebTitle}>
            {" "}
            <h3>LabLeb</h3>
          </div>
          <div className={classes.description}>
            <h5 style={{ padding: "40px" }}>
              Lableb Cloud Search is a fully functional search application added
              to your website. It helps you get the right answer to more people
              on your platform and surface relevant content for your readers and
              followers. The search results are fresh, complete, and relevant to
              your audience. Full-text Search supports (Arabic – English)
              languages and popular search features such as highlighting,
              autocomplete, and geospatial search. Lableb Cloud Search is a
              product of <br />
              <a href="default.asp" target="_blank">
                the Arabic Search Engine Lableb
              </a>
            </h5>
          </div>
        </div>

        <div className={classes.col}>
          <div className={classes.linksHeader}>
            <h3>Additional Links:</h3>
            <div className={classes.links}>
              <h5>Search Engine</h5>
              <h5>About</h5>
              <h5>Blog</h5>
              <h5>Careers</h5>
              <br />
              <h5>Arabic</h5>
              <h5>English</h5>
            </div>
          </div>
        </div>

        <div className={classes.col}>
          <div className={classes.contactInfo}>
            <h5>For questions or enquiries please click on the link below:</h5>
            <h5 className={classes.contactUs}>Contact us</h5>
          </div>
        </div>
      </div>
    );
  }
}

Info.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Info);
