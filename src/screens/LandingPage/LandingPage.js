import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AsyncLoader from "../../hoc/AsyncLoader";
import { styles as customStyles } from "./styles";
const styles = customStyles;

const AsyncAppBar = AsyncLoader(() => {
  return import("../../components/LablebAppBar/LablebAppBar");
});

const AsyncHeadSection = AsyncLoader(() => {
  return import("../../organisms/HeadSection/HeadSection");
});

const AsyncProducts = AsyncLoader(() => {
  return import("../../organisms/Products/Products");
});

const AsyncInfo = AsyncLoader(() => {
  return import("../../organisms/Info/Info");
});
class LandingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <AsyncAppBar />
        <AsyncHeadSection />
        <AsyncProducts />
        <AsyncInfo />
      </div>
    );
  }
}

LandingPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LandingPage);
