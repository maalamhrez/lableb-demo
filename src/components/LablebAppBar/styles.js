import React from "react";
import { withStyles } from "@material-ui/core/styles";
export const styles = (theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
});
