import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { styles as customStyles } from "./styles";
const styles = customStyles;

class LablebAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (
      <div>
        <AppBar position="fixed" color="white" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" noWrap>
              LabLeb
            </Typography>
            <IconButton style={{ marginLeft: "auto", color: "black" }}>
              {"Login"}
            </IconButton>
            <IconButton style={{ color: "black" }}>{"Main"}</IconButton>
            <IconButton style={{ color: "black" }}>{"Any"}</IconButton>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

LablebAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LablebAppBar);
