import React from "react";
import logo from "./logo.svg";
import "./App.css";
import LandingPage from "./screens/LandingPage/LandingPage";
function App() {
  return (
    <div className="App">
      <LandingPage />
    </div>
  );
}

export default App;
